(function() {
	'use strict';

	angular
	.module('lagom')
	.directive('switch', function() {
		return {
			scope: {
				on: '=?'
			},
			restrict: 'C',
			replace: true,
			templateUrl: 'switch.html',
			link: function(scope, element, attrs) {
				var isActive;

				if(scope.on === true) turnOn();
				else turnOff();

				scope.toggle = function() {
					if(isActive) return turnOff();
					turnOn();
				};

				function turnOn() {
					isActive = true;
					element.attr('active','');
					scope.on = true;
				}

				function turnOff() {
					isActive = false;
					element.removeAttr('active');
					scope.on = false;
				}
			}
		};
	});
})();