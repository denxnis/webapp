(function() {
	'use strict';

<!--  Disabled feature that disables the button and makes it 100% transparent -->
	angular.module('lagom').directive('loader', function($location) {
		return {
			restrict: 'AE',
			replace: false,
			priority: 1,
			scope: {
				onLoadCallback: '&',
				control: '=',
				small: '&',
				timer: '=',
				color: '@'
			},
			templateUrl: 'loader.html',
			link: function(scope, element, attrs) {
				var timeoutID;
				var timer = scope.timer == null ? 1000 : scope.timer;
				scope.small = 'small' in attrs;
				scope.color = 'color' in attrs && typeof attrs.color != 'undefined' ? attrs.color : '#475055';
				scope.internalControl = scope.control || {};
				scope.animate = false;

				scope.internalControl.hoverIn = function() {
					scope.animate = true;

					timeoutID = setTimeout(function() {
						scope.$apply(function() {
							scope.onLoadCallback();
						});
					}, timer);
				};

				scope.internalControl.hoverOut = function() {
					scope.animate = false;
					clearTimeout(timeoutID);
				};

				scope.circle = {
					cx: 40,
					cy: scope.small ? 60 : 50,
					r: scope.small ? 25 : 35,
					stroke: scope.color,
					opacity: scope.small ? 0.6 : 1.0
				}
			}
		};
	});
})();
