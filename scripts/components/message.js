(function() {
  angular.module('lagom')
  .component('message', {
    template: '<h1>Hello this is a component by {{$ctr.user.name}}</h1>',
    controller: function() {
      this.user = {
        name: 'Dennis'
      };
    }
  });
});
