(function() {
	angular.module('lagom')
	.config(function($httpProvider) {
		$httpProvider.interceptors.push('authInterceptor');
	})
	// .constant('API_URL', 'http://localhost:3000/')
	.constant('API_URL', 'https://lagom.life/api/')
	.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
	    cfpLoadingBarProvider.latencyThreshold = 500;
	}])
	.config(['toastyConfigProvider', function(toastyConfigProvider) {
    toastyConfigProvider.setConfig({
        sound: false,
        showClose: false,
        clickToClose: true,
        timeout: 5000
    });
	}]);
})();
