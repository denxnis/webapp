(function() {
	'use strict';

	angular.module('lagom').factory('dataService', function($http, API_URL) {
		return {
			addPrivateUser: function(topicId, username) {
				var request = {
					method: 'POST',
					url: API_URL + 'topic/privateAdd',
					headers: { 'Content-Type': 'application/json' },
					data: { topicId: topicId, username: username }
				};
				return $http(request);
			},

			addResponse: function(topicId, response) {
				var request = {
					method: 'POST',
					url: API_URL + 'response',
					headers: { 'Content-Type': 'application/json' },
					data: { topicId: topicId, response: response }
				};
				return $http(request);
			},

			addTopic: function(name, content, isPublic) {
				var request = {
					method: 'POST',
					url: API_URL + 'topic',
					headers: { 'Content-Type': 'application/json' },
					data: { name: name, content: content, public: isPublic }
				};
				return $http(request);
			},

			changePassword: function(oldPassword, newPassword) {
				var request = {
					method: 'POST',
					url: API_URL + 'change-password',
					headers: { 'Content-Type': 'application/json' },
					data: { oldPassword: oldPassword, newPassword: newPassword }
				};
				return $http(request);
			},

			contactUs: function(name, email, message) {
				var request = {
					method: 'POST',
					url: API_URL + 'contact',
					headers: { 'Content-Type': 'application/json' },
					data: { name: name, email: email, message: message }
				};
				return $http(request);
			},

			deleteResponse: function(messageId) {
					var request = {
						method: 'DELETE',
						url: API_URL + 'response',
						params: {messageId: messageId}
					};
					return $http(request);
			},

			deleteTopic: function(topicId) {
				var request = {
					method: 'DELETE',
					url: API_URL + 'topic',
					params: {topicId: topicId}
				};
				return $http(request);
			},

			editResponse: function(responseId, responseMessage) {
				var request = {
					method: 'POST',
					url: API_URL + 'editResponse',
					headers: { 'Content-Type': 'application/json' },
					data: { messageId: responseId, messageContent: responseMessage }
				};
				return $http(request);
			},

			getPrivateUsers: function(topicId) {
				var request = {
					method: 'GET',
					url: API_URL + 'topicUsers',
					headers: { 'Content-Type': 'application/json' },
					params: {topicId: topicId}
				};
				return $http(request);
			},

			getTopics: function() {
				var request = {
					method: 'GET',
					url: API_URL + 'topics',
					headers: { 'Content-Type': 'application/json' }
				};
				return $http(request);
			},

			getConversation: function(topicId) {
				var request = {
					method: 'POST',
					url: API_URL + 'conversation',
					headers: { 'Content-Type': 'application/json' },
					data: { topicId: topicId }
				};
				return $http(request);
			},

			login: function(username, password) {
				var request = {
					method: 'POST',
					url: API_URL + 'login',
					headers: { 'Content-Type': 'application/json' },
					data: { username: username, password: password }
				};
				return $http(request);
			},

			register: function(username, password) {
				var request = {
					method: 'POST',
					url: API_URL + 'register',
					headers: { 'Content-Type': 'application/json' },
					data: { username: username, password: password }
				};
				return $http(request);
			},

			removePrivateUser: function(topicId, username) {
				var request = {
					method: 'POST',
					url: API_URL + 'topic/privateRemove',
					headers: { 'Content-Type': 'application/json' },
					data: { topicId: topicId, username: username }
				};
				return $http(request);
			},

			userLookup: function(username) {
				var request = {
					method: 'POST',
					url: API_URL + 'exists',
					headers: { 'Content-Type': 'application/json' },
					data: { username: username }
				};
				return $http(request);
			}
		};
	});
})();
