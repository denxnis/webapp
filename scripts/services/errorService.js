(function() {
  'use strict';

  angular.module('lagom').factory('errorService', function ($location) {
    var errorMessage = '';
    var errorCode = '';

    function setErrorMessage(errorMessage) {
      this.errorMessage = errorMessage;
    }

    function setErrorCode(errorCode) {
      this.errorCode = errorCode;
    }

    function getErrorMessage(){
      return this.errorMessage;
    }

    function getErrorCode() {
      return this.errorCode;
    }

    function redirect() {
      $location.path('/error');
    }
    
    return {
      setErrorMessage: setErrorMessage,
      setErrorCode: setErrorCode,
      getErrorMessage: getErrorMessage,
      getErrorCode: getErrorCode,
      redirect: redirect
    };
  });
})();  