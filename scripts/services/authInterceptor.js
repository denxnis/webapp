(function() {
  'use strict';

  angular.module('lagom').factory('authInterceptor', function (tokenService, userService) {
      return {
        request: function (config) {
          var token = tokenService.getToken();

          if(token)
            config.headers.authorization = 'Bearer ' + token;

          return config;
        },
        response: function(response) {
          var token = response.headers('authorization');

          if(response.headers('Authorization-Error')) {
            tokenService.removeToken();
            userService.clear();
          }

          if(token) {
            token = token.split(' ')[1];
            tokenService.setToken(token);
          }
          
          return response;
        }
      };
  });
})();  