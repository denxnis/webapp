(function() {
	'use strict';

	angular.module('lagom').service('userService', ['$window', function($window) {
		var storage = $window.localStorage;
		var username;
		var groups = [];
		var visited;

		this.setUsername = function(username) {
			this.username = username;
			storage.setItem('username', username);
		};

		this.setVisited = function() {
			this.visited = true;
			storage.setItem('visited', true);
		};

		this.getVisited = function() {
			if(!visited) visited = storage.getItem('visited');
			return visited;
		};
 
		this.getUsername = function() {
			if(!username) username = storage.getItem('username');
			return username;
		};

		this.setGroups = function(groups) {
			this.groups = groups;
			storage.setItem('userGroups', this.groups);
		};

		this.getUserGroups = function() {
			return groups;
		};

		this.clear = function() {
			username = null;
			groups = [];

			storage.removeItem('username');
			storage.removeItem('userGroups');
		};
	}]);
})();