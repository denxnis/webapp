(function() {
	'use strict';

	angular.module('lagom').service('tokenService', ['$window', function($window) {
		var storage = $window.localStorage;
		var cachedToken;

		this.setToken = function(token) {
			cachedToken = token;
			storage.setItem('userToken', token);
		};

		this.getToken = function() {
			if(!cachedToken) cachedToken = storage.getItem('userToken');
			return cachedToken;
		};

		this.removeToken = function() {
			cachedToken = null;
			storage.removeItem('userToken');
		};

		this.isAuthenticated = function() {
			if(cachedToken) {
				console.log('logged in');
				return true;
			}
			else {
				console.log('logged out');
				return false;
			}
		};
	}]);
})();	