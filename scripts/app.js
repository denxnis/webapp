(function() {
	'use strict';

	angular.module('lagom', ['angular-loading-bar', 'angular-toasty', 'ngRoute', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'templatecache', 'offClick', 'relativeDate'])
	.config(function ($routeProvider, $locationProvider) {
		$routeProvider
		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'homeCtrl',
			controllerAs: 'home'
		})
		.when('/social', {
			templateUrl: 'views/social.html',
			controller: 'socialCtrl',
			controllerAs: 'social'
		})
		.when('/conversation/:topicId', {
			templateUrl: 'views/conversation.html',
			controller: 'conversationCtrl',
			controllerAs: 'conversation'
		})
		.when('/contact', {
			templateUrl: 'views/contact.html',
			controller: 'contactCtrl',
			controllerAs: 'contact'
		})
		.when('/profile', {
			templateUrl: 'views/profile.html',
			controller: 'loginCtrl',
			controllerAs: 'login'
		})
		.when('/error', {
			templateUrl: 'views/error.html',
			controller: 'errorCtrl',
			controllerAs: 'error'
		})
		.otherwise({redirectTo: '/'});
	})
	.run(function (tokenService, userService, $templateCache, $location, $window) {
		var isRoot = $location.path() === "/" || $location.path() === "";

		/* Redirect existing users to social page */
		if(userService.getVisited() && isRoot) {
			$location.path('/social');
		}
	});
})();
