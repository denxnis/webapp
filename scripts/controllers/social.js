(function() {
	'use strict';

	angular.module('lagom')
	.controller('socialCtrl', function($scope, $http, $location, dataService, tokenService, userService, $routeParams, toasty) {
		var vm = this;
		
		vm.topicName = null;
		vm.topics = [];
		vm.username = null;
		vm.topicContent = null;
		vm.isLoggedIn = false;
		vm.showNewTopicDialogue = false;
		vm.privateTopic = true;

		//this is a hack
		//elaborate
		var numElements = 100;
		vm.temp = Array.apply(null, Array(numElements)).map(function () { return new Object(); });

		//Watch logged-in status
		$scope.$watch(function() { 
			return userService.getUsername(); 
		}, function(newVal, oldVal) {
			vm.username = newVal;

			if(newVal) vm.isLoggedIn = true;
			else vm.isLoggedIn = false;
			getTopics();

		}, true);

		vm.redirect = function(topicId, topicName){
			$location.path("/conversation/" + topicId);
		};

		//Create new topic
		vm.submitTopic = function() {
			dataService.addTopic(vm.topicName, vm.topicContent, !vm.privateTopic)
				.then(function(response) {
					vm.toggleTopicDialogue();
					getTopics();
				})
				.catch(function(error) {
					console.log(error);
					toasty.error({ title: 'Issue creating thread', msg: error.data.message });
				});
		};
		
		//Toggle dialogue box for creating a new topic
		vm.toggleTopicDialogue = function() {
			vm.showNewTopicDialogue = !vm.showNewTopicDialogue;
			vm.topicName = null;
			vm.topicContent = null;
		};

		//Retrieve all topics
		function getTopics() {
			dataService.getTopics()
				.then(function(response){
					vm.topics = response.data;
				})
				.catch(function(error) { console.log(error); });
		}

		//Cancel creating new topic
		vm.cancel = function() {
			vm.toggleTopicDialogue();
		};
	});
})();	