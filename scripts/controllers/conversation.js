(function() {
	'use strict';

	angular.module('lagom')
	.controller('conversationCtrl', function($scope, $window, $http, $timeout, $location, $sce, $templateCache, $compile, tokenService, userService, errorService, $routeParams, toasty, $uibModal, dataService) {
		var vm = this;

		vm.topicId = $routeParams.topicId;
		vm.topicAuthor = null;
		vm.topicName = '';

		vm.popoverIsOpen = false;

		vm.isLoggedIn = false;
		vm.username = null;

		vm.conversation = [];
		vm.privateUsers = [];
	  vm.templateUrl = 'addUser.html';

		//Watch logged-in status
		$scope.$watch(function() {
			return userService.getUsername();
		}, function(newVal, oldVal) {
				if(newVal) {
					vm.isLoggedIn = true;
					vm.username = newVal;
				} else {
					vm.isLoggedIn = false;
					vm.username = null;

					if(vm.privateUsers.length > 0)
						$location.path('/social');
				}
		}, true);

		//Retrieve conversation
		function getConversation(topicId) {
			dataService.getConversation(topicId)
				.then(function(response) {
					vm.topicName = response.data.topicName;
					vm.conversation = response.data.data;
					vm.topicAuthor = response.data.data[0].author;
				})
				.catch(function(error) {
					console.dir(error);
					errorService.setErrorMessage(error.data.message);
					errorService.setErrorCode(error.status);
					errorService.redirect();
				});
		}

		function getPrivateUsers(topicId) {
			dataService.getPrivateUsers(topicId)
				.then(function(response) {
					vm.privateUsers = response.data;
				})
				.catch(function(error) {
					console.log(error.data);
				});
		}

		vm.showPopOver = function() {
			return vm.popoverIsOpen;
		}

		vm.togglePopOver = function() {
			vm.popoverIsOpen = !vm.popoverIsOpen;
		}

		vm.showLock = function() {
			if(vm.username == vm.topicAuthor) return true;
			if(vm.privateUsers.length === 0) return false;
			return true;
		};

		vm.showTrash = function() {
			if(vm.username == vm.topicAuthor) return true;
			return false;
		};

		//Restrict topic to specific users
		vm.addUser = function(username) {
			if(vm.privateUsers.indexOf(username) != -1) {
				return;
			}

			dataService.addPrivateUser(vm.topicId, username)
				.then(function(response) {
					getPrivateUsers(vm.topicId);
					toasty.success({ msg: 'User added successfully' });
				})
				.catch(function(error) {
					toasty.error({ title: 'Add user failed', msg: 'User may not exist' });
				});
		};

		vm.removeUser = function(index) {
			dataService.removePrivateUser(vm.topicId, vm.privateUsers[index])
				.then(function(response) {
					getPrivateUsers(vm.topicId);
					toasty.warning({msg: 'User removed successfully'});
				})
				.catch(function(error) {
					toasty.error({title: 'Remove user failed', msg: error.message });
				});
		};

		vm.back = function() {
			$location.path('/social');
		};

		vm.openModal = function(){
		  vm.modalInstance = $uibModal.open({
		      templateUrl: 'deleteTopicModal.html',
		      animation: true,
		      scope: $scope
		  });
		};

		vm.removeTopic = function() {
			dataService.deleteTopic(vm.topicId)
			.then(function(response) {
				$location.path('/social');
			})
			.catch(function(error) {
				console.error(error);
			});
		};

		/* submit a response */
		vm.respond = function(topicId) {
			toasty.clear();

			dataService.addResponse(topicId, vm.responseContent)
				.then(function(response) {
					vm.responseContent = null;
					getConversation(topicId);
				})
				.catch(function(error) {
					console.error(error);
					toasty.error({ msg: error.data.message });
				});
		};

		vm.editMode = null;

		/* return whether a post is editable by the current user */
		vm.isEditable = function(index, author) {
			return vm.username === author;
			//return vm.username === author && (vm.editMode == null || vm.editMode === index);
		};

		/* return whether a post is deletable by the current user */
		vm.isDeletable = function(index, author) {
			return vm.username === author && index > 0;
		}

		/* return whether a post should be in edit mode */
		vm.isEditMode = function(index) {
			return index === vm.editMode;
		}

		vm.save = function(index, messageId) {
			console.log('save')
			if(index === vm.editMode) {
				var tmp = document.querySelectorAll('.conversation td textarea.message');
				var content = tmp[index].value;

				saveChanges(messageId, content)
			}
		}

		vm.edit = function(index, messageId) {
			console.log('edit')
			var tmp = document.querySelectorAll('.conversation td div.message');
			var content = tmp[index].innerText;

			vm.editContent = content;
			vm.editMode = index;
		}

		vm.deletePost = function(messageId) {
			toasty.clear();

			dataService.deleteResponse(messageId)
				.then(function(response) {
					vm.editMode = null;
					getConversation(vm.topicId);
				})
				.catch(function(error) {
					console.error(error);
					toasty.error({ msg: error.data.message });
				})
		}

		var saveChanges = function(messageId, response) {
			toasty.clear();

			dataService.editResponse(messageId, response)
				.then(function(response) {
					vm.editMode = null;
					getConversation(vm.topicId);
				})
				.catch(function(error) {
					toasty.error({
						title: 'Issue editing response',
						msg: error.data.message
					});
					console.log(error);
				});
		};

		getConversation(vm.topicId);
		getPrivateUsers(vm.topicId);
	});
})();
