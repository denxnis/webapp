(function() {
	'use strict';

	angular.module('lagom')
	.controller('errorCtrl', function(errorService) {
		var vm = this;

		vm.errorMessage = errorService.getErrorMessage();
		vm.errorCode = errorService.getErrorCode();

		console.log(vm.errorCode);
	});
})();