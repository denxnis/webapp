(function() {
	'use strict';

	angular.module('lagom').controller('loginCtrl', function($scope, $http, $timeout, $uibModal, toasty, dataService, tokenService, userService) {
		var vm = this;
		vm.hasError = false;
		
		vm.user = {
			username: null,
			password: null,
			verifyPassword: null,
			email: null,
			errors: {}
		};

		vm.passwords = {
			currentPassword: null,
			newPassword: null,
			verifyNewPassword: null,
			errors: {}
		};

		var ALPHA_NUMBERIC_REGEX = /^\w+$/;

		vm.openModal=function() {
		  vm.modalInstance = $uibModal.open({
		  	templateUrl: 'updatePasswordModal.html',
		    animation: true,
		    controller: 'loginCtrl',
		    scope: $scope,
		    size: 'sm'
		  });
		};

		vm.openRegisterModal = function() {
			vm.modalInstance = $uibModal.open({
				templateUrl: 'registerModal.html',
				animation: true,
				controller: 'loginCtrl',
				scope: $scope,
				size: 'sm'
			});
		};

		/* LOGIN BUTTON */
		vm.login = function() {
			dataService.login(vm.username, vm.password)
			.then(function(response) {
				loginSuccess(response.data.token, response.data.user);
			})
			.catch(function(error) {
				loginError(error.data.message, error.data.title);
			});
		};

		/* REGISTER BUTTON */
		vm.register = function(user) {
			var validUsername,
					validPasswords,
					validPassword,
					validEmail;

			//Clear errors
			user.errors.username = false;
			user.errors.password = false;
			user.errors.verifyPassword = false;
			user.errors.email = false;

			toasty.clear(); // clear any remaining messages

			//Validate input on client-side
			validUsername = isValidUsername(user.username);
			validPasswords = matchingPasswords(user.password, user.verifyPassword);
			validPassword = isValidPassword(user.password);

			if(validUsername !== true) {
				user.errors.username = true;
				toasty.error({ msg: validUsername});
				return;
			} else {
				dataService.userLookup(user.username)
				.then(function(response) {
					if(response.data.userExists) {
						user.errors.username = true;
						toasty.error({ msg: 'Username already taken'});
						return;
					}
					registerContinued();
				})
				.catch(function(error) {
					toasty.error({ msg: 'Something went wrong...'});
					console.error(error);
				});
			}

			//Nested function called after username validation passes
			function registerContinued() {
				if(validPassword !== true) {
					user.errors.password = true;
					user.errors.verifyPassword = true;
					toasty.error({ msg: validPassword});
					return;
				}

				if(validPasswords !== true) {
					user.errors.verifyPassword = true;
					user.errors.password = true;
					toasty.error({ msg: validPasswords});
					return;
				}

				if(user.errors.username || user.errors.password || user.errors.verifyPassword || user.errors.email )
					return 1;

				dataService.register(user.username, user.password)
					.then(function(response) {
						vm.modalInstance.dismiss();
						toasty.success({ title: 'Success', msg: 'Registration complete'});
						loginSuccess(response.data.token, response.data.user);
					})
					.catch(function(error) {
						if(error.data.code == 400) user.errors.username = true;
						if(error.data.code == 403) user.errors.password = true;
						console.dir(error)
						loginError(error.data.msg);
					});
			}
		};

		function loginError(message, title) {
			userService.clear();
			vm.hasError = true;
			
			//Clear notifications
			toasty.clear();
			
			if(arguments.length > 1) 
				return toasty.error({title: title, msg: message});
			
			toasty.error({msg: message});
		}

		function loginSuccess(token, username) {
			tokenService.setToken(token);
			userService.setUsername(username);

			vm.username = "";
			vm.password = "";
			vm.hasError = false;
		}

		vm.close = function() {
			vm.modalInstance.dismiss();
		};

		/* LOGOUT BUTTON */
		vm.logout = function() {
			tokenService.removeToken();
			userService.clear();
			vm.username = "";
			vm.password = "";
		};

		vm.changePassword = function(passwords) {
			var validCurrentPassword,
					validNewPassword,
					validPasswords;

			//Clear errors
			passwords.errors.currentPassword = false;
			passwords.errors.newPassword = false;
			passwords.errors.verifyPassword = false;

			toasty.clear(); // clear any remaining messages

			//Validate input on client-side
			validCurrentPassword = isValidPassword(passwords.currentPassword);
			validNewPassword = isValidPassword(passwords.newPassword);
			validPasswords = matchingPasswords(passwords.newPassword, passwords.verifyPassword);

			if(validCurrentPassword !== true) {
				passwords.errors.currentPassword = true;
				toasty.error({msg: validCurrentPassword});
				return;
			}

			if(validNewPassword !== true) {
				passwords.errors.newPassword = true;
				toasty.error({msg: validNewPassword});
				return;
			}

			if(validPasswords !== true) {
				passwords.errors.newPassword = true;
				passwords.errors.verifyPassword = true;
				toasty.error({msg: validPasswords});
				return;
			}

			if(passwords.errors.currentPassword || passwords.errors.newPassword || passwords.errors.verifyPassword)
				return;

			dataService.changePassword(passwords.currentPassword, passwords.newPassword)
				.then(function(response) {
					vm.modalInstance.dismiss();
					toasty.success({title: 'Success', msg: 'Password update complete'});
				})
				.catch(function(error) {
					if(error.data.code == 401) passwords.errors.currentPassword = true;
					if(error.data.code == 403) passwords.errors.newPassword = true;
					toasty.error({msg: error.data.message});
				});
		};

		/* WATCH FOR LOGGED IN STATUS CHANGE */
		$scope.$watch(function() { return userService.getUsername(); }, function(newValue, oldValue) {
			vm.loggedInUser = userService.getUsername();
		}, true);

		function isValidUsername(username) {
			if(username === null) return 'Username cannot be empty';
			if(!ALPHA_NUMBERIC_REGEX.test(username)) return 'Username must be alphanumeric';
			return true;
		}

		function matchingPasswords(password, verifyPassword) {
			if(password !== verifyPassword) return 'Passwords do not match';
			return true;
		}

		function isValidPassword(password) {
			if(password === null || password === '') return 'Password field cannot be empty';
			if(password.length > 1000) return 'Invalid password';
			return true;
		}

		/* Workaround for login-container rendering issue */
		var loginContainer = document.getElementById("login-container");
		loginContainer.className = "login-container";

		/* Display login container opened if first visit */
		if(!userService.getVisited()) {
			loginContainer.className = "login-container active";

			userService.setVisited();

			$timeout(function () {
				document.getElementById("login-container").className = "login-container";
			}, 3000);
		}
	});
})();