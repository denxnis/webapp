(function() {
	'use strict';

	angular.module('lagom').controller('contactCtrl', function($http, $uibModal, toasty, dataService) {
		var EMAIL_REGEX = /[^\s@]+@[^\s@]+\.[^\s@]+/;
		var vm = this;

		vm.errors = {
			name: false,
			email: false,
			message: false
		};

		vm.openModal=function(){
		  vm.modalInstance=$uibModal.open({
	      templateUrl: 'contactUsModal.html',
	      animation: true
		  });
		};

		vm.submit = function() {
			toasty.clear(); // clear any remaining messages
			
			//begin validation
			if(!angular.isDefined(vm.name) || vm.name === '') {				
				vm.errors.name = true;
				return toasty.error({ msg: 'Name cannot be empty'});
			}
			else
				vm.errors.name = false;

			if(!angular.isDefined(vm.email) || vm.email === '' || !EMAIL_REGEX.test(vm.email)) {
				vm.errors.email = true;
				return toasty.error({msg: 'Not a valid email'});
			}
			else {
				vm.errors.email = false;
			}
				
			if(!angular.isDefined(vm.message) || vm.message === '') {
				vm.errors.message = true;
				return toasty.error({msg: 'Message cannot be empty'});
			}
			else {
				vm.errors.message = false;
			}

			dataService.contactUs(vm.name, vm.email, vm.message)
				.then(function(response) {
					vm.openModal();
					vm.name = undefined;
					vm.email = undefined;
					vm.message = undefined;
				})
				.catch(function(error) {
					console.dir(error)
					if(error.code === 400) vm.errors.name = true;
					if(error.code === 401) vm.errors.email = true;
					if(error.code === 402) vm.errors.message = true;
					toasty.error({msg: error.data.message});					
				});
		};
	});
})();