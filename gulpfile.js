// Include gulp & plugins
var gulp = require('gulp'),
  jshint = require('gulp-jshint'),
 	livereload = require('gulp-livereload'),
 	connect = require('gulp-connect'),
 	open = require('gulp-open'),
  concat = require('gulp-concat'),
  templateCache = require('gulp-angular-templatecache');

gulp.task('lint', function() {
  return gulp.src('scripts/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default', { verbose: true }));
});

// Webserver
gulp.task('connect', function() {
	connect.server({
		root: '',
		port: 8080,
		livereload: true, 
    start: true
	});
});

gulp.task('templates', function () {
  return gulp.src('templates/*.html')
    .pipe(templateCache('templates.js', {module:'templatecache', standalone:true}))
    .pipe(gulp.dest('./scripts'));
});

gulp.task('html', function () {
  gulp.src(['scripts/controllers/*.js', 
  			'scripts/services/*.js',
        'scripts/components/*.js',
        'scripts/components/*.html',
  			'scripts/*.js',
  			'styles/*.css',
  			'styles/**/*.css',
  			'views/*.html',
    		'index.html'])
    .pipe(connect.reload());
});

// Launch app
gulp.task('url', function() {
  gulp.src(__filename)
  .pipe(open({uri: 'http://localhost:8080/'}));
});

// Watch files for changes
gulp.task('watch', function() {
    gulp.watch([
        'templates/*.html',
        'scripts/controllers/*.js',
  			'scripts/services/*.js',
        'scripts/components/*.js',
        'scripts/components/*.html',
  			'scripts/*.js',
  			'styles/*.css',
  			'styles/**/*.css',
  			'views/*.html',
    		'index.html'
      ], ['templates', 'concat','html']);
});

// Concat JavaScript files
gulp.task('concat', function() {  
  return gulp.src([
      './bower_components/angular/angular.min.js',
      './bower_components/angular-sanitize/angular-sanitize.min.js',
      './bower_components/angular-animate/angular-animate.min.js',
      './bower_components/angular-loading-bar/build/loading-bar.min.js',
      './bower_components/angular-route/angular-route.min.js',
      './bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
      './bower_components/angular-toasty/dist/angular-toasty.min.js',
      './bower_components/angular-off-click/dist/angular-off-click.min.js',
      './bower_components/angular-relative-date/dist/angular-relative-date.js',
      './scripts/templates.js',
      './scripts/app.js',
      './scripts/app.config.js',
      './scripts/services/*.js',
      './scripts/controllers/*.js',
      './scripts/components/*.js'
    ])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./dist/'));
});

// Default tasks
gulp.task('default', ['templates', 'watch', 'connect', 'url', 'concat']);

